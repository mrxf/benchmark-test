const benchmark = require('benchmark');
const suite = new benchmark.Suite;
const quickSort = require('./quick-sort.js');

suite.add('排序#sort()', function() {
    const randArray = [...Array(10000)].map(v => Math.ceil(Math.random() * 10000));
    let sortedArray = randArray.sort((a, b) => a - b);
});
suite.add('排序#快速排序', function() {
    const randArray = [...Array(10000)].map(v => Math.ceil(Math.random() * 10000));
    let sortedArray = quickSort(randArray, 0, randArray.length - 1);
})

.on('cycle', function(event) {
    console.log(String(event.target));
})
.on('complete', function() {
    console.log('运行速度最快的是' + this.filter('fastest').map('name'));
})
.run();