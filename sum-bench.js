const benchmark = require('benchmark');
const suite = new benchmark.Suite;

/**
 * 测试reduce和for循环计算加和的性能
 */

suite.add('getSum#reduce', function() {
    const aNums = [...Array(100000)].map(v => Math.ceil(Math.random() * 100000));
    let sum = aNums.reduce((pre, curr) => pre + curr, 0);
})

suite.add('getSum#for', function() {
    const aNums = [...Array(100000)].map(v => Math.ceil(Math.random() * 100000));
    let sum = 0;
    for(let i= 0; i< aNums.length; i++) {
        return sum += aNums[i];
    }
})

.on('cycle', function(event) {
    console.log(String(event.target));
})
.on('complete', function() {
    console.log('Fastest is ' + this.filter('fastest').map('name'));
})
.run();