
/**
 * 递推法求斐波那契数列方法
 * @param {number} n 第n个
 */
function fibonacci(n) {
    const aFi = new Array(n+1);
    aFi[0] = 0; aFi[1] = 1;
    for(let i=2; i<= n; i++){
        aFi[i] = aFi[i-1] + aFi[i-2];
    }

    return aFi[n];
}

for(let i=0;i<10;i++){
    console.log(fibonacci(i))
}

/*
 * 时间复杂度 O(n)，但是有太多的内存开销，无意义的数据 
 */