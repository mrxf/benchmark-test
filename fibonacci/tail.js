'use strict'

/* 开启严格模式才会使用尾调优化 */

/**
 * 尾调法求斐波那契数
 * @param {number} n 第n个数字
 * @param {number} current 当前结果
 * @param {number} next 下一个值
 */
function fibonacci(n, current = 0, next = 1) {
    if(n === 1) return next;
    if(n === 0) return 0;
    // console.log(`fibonacci(${n}, ${next}, ${current + next})`);
    return fibonacci(n - 1, next, current + next);
}

for(let i=0;i<10;i++){
    console.log(fibonacci(i))
}