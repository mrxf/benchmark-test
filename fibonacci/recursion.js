/**
 * 递归法求斐波那契数
 * @param {number} n 第n个
 */
function fibonacci(n){
    if(n === 1 || n === 0 ) return n;
    console.log(`fibonacci(${n-1}) + fibonacci(${n-2})`)
    return fibonacci(n-1) + fibonacci(n-2);
}

// for(let i=0;i<10;i++){
//     console.log(fibonacci(i))
// }
fibonacci(6);