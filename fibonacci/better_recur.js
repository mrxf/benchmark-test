
/**
 * 优化递推法求斐波那契数
 * @param {number} n 第n个数字
 */
function fibonacci(n) {
    let current = 0;
    let next = 1;
    let temp;
    // while(n-->0){
    //     temp = current;
    //     current = next;
    //     next += temp;
    // }
    // for(let i = 0; i < n; i++){
    //     temp = current;
    //     current = next;
    //     next += temp;
    // }
    for(let i = 0; i < n; i++){
        [current, next] = [next, current + next];
    }
    return current;
}

for(let i=0;i<10;i++){
    console.log(fibonacci(i))
}