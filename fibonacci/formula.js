/**
 * 使用通项公式法计算斐波那契
 * @param {number} n 第n个斐波那契数
 */
function fibonacci(n) {
    const SQRT_FIVE = Math.sqrt(5);
    return Math.round(1/SQRT_FIVE * (Math.pow(0.5 + SQRT_FIVE/2, n) - Math.pow(0.5 - SQRT_FIVE/2, n)));
}

for(let i=0;i<10;i++){
    console.log(fibonacci(i))
}